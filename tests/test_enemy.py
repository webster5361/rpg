#!/usr/local/bin/python3

import unittest
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from lib.enemy import Enemy

class TestEnemy(unittest.TestCase):

    def test_init_health(self):
        enemy = Enemy('Odin', 35, 50, 50, 50, 50)
        self.assertEqual(enemy.health, 50)

    def test_init_max_health(self):
        enemy = Enemy('Odin', 35, 50, 50, 50, 50)
        self.assertEqual(enemy.max_health, 50)

    def test_init_age(self):
        enemy = Enemy('Odin', 35, 50, 50, 50, 50)
        self.assertEqual(enemy.age, 35)

    def test_init_level(self):
        enemy = Enemy('Odin', 35, 50, 50, 50, 50)
        self.assertEqual(enemy.level, 1)

    def test_init_status(self):
        enemy = Enemy('Odin', 35, 50, 50, 50, 50)
        self.assertEqual(enemy.status.value, 'ALIVE')

    def test_reduce_health_normal(self):
        enemy = Enemy('Odin', 35, 50, 50, 50, 50)
        self.assertEqual(enemy.reduce_health(25), 25)

    def test_reduce_health_zero(self):
        enemy = Enemy('Odin', 35, 50, 50, 50, 50)
        self.assertEqual(enemy.reduce_health(0), 50)

    def test_reduce_health_big(self):
        enemy = Enemy('Odin', 35, 50, 50, 50, 50)
        self.assertEqual(enemy.reduce_health(5000), 0)

    def test_add_health_normal(self):
        enemy = Enemy('Odin', 35, 5, 50, 50, 50)
        self.assertEqual(enemy.add_health(27), 32)

    def test_add_health_zero(self):
        enemy = Enemy('Odin', 35, 5, 50, 50, 50)
        self.assertEqual(enemy.add_health(0), 5)

    def test_add_health_zero(self):
        enemy = Enemy('Odin', 35, 5, 50, 50, 50)
        self.assertEqual(enemy.add_health(100), 50)

    def test_add_magic_normal(self):
        enemy = Enemy('Odin', 35, 5, 50, 10, 50)
        self.assertEqual(enemy.add_magic(20), 30)

    def test_add_magic_zero(self):
        enemy = Enemy('Odin', 35, 5, 50, 50, 50)
        self.assertEqual(enemy.add_magic(0), 50)

    def test_add_magic_big(self):
        enemy = Enemy('Odin', 35, 5, 50, 40, 50)
        self.assertEqual(enemy.add_magic(2000), 50)

    def test_reduce_magic_normal(self):
        enemy = Enemy('Odin', 35, 5, 50, 50, 50)
        self.assertEqual(enemy.reduce_magic(20), 30)
        
    def test_reduce_magic_zero(self):
        enemy = Enemy('Odin', 35, 5, 50, 40, 50)
        self.assertEqual(enemy.reduce_magic(20), 20)

    def test_reduce_magic_big(self):
        enemy = Enemy('Odin', 35, 5, 50, 50, 50)
        self.assertEqual(enemy.reduce_magic(2000), 0)

if __name__ == '__main__':
    unittest.main()