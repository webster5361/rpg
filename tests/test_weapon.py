#!/usr/local/bin/python3

import unittest
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from lib.weapon import Weapon

class TestWeapon(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestWeapon, self).__init__(*args, **kwargs)
        self.weapon_name = 'Simple Dagger'
        self.weapon_desc = 'A simple, rusted dagger.'

    def test_init_durability(self):
        leather_weapon = Weapon(self.weapon_name, 1.0, 5, self.weapon_desc, 5, 100.0)
        self.assertEqual(leather_weapon.durability, 100.0)

    def test_init_max_durability(self):
        leather_weapon = Weapon(self.weapon_name, 1.0, 5, self.weapon_desc, 5, 100.0)
        self.assertEqual(leather_weapon.max_durability, 100.0)

    def test_init_status(self):
        leather_weapon = Weapon(self.weapon_name, 1.0, 5, self.weapon_desc, 5, 100.0)
        self.assertEqual(leather_weapon.status.value, 'REPAIRED')

    def test_init_defense(self):
        leather_weapon = Weapon(self.weapon_name, 1.0, 5, self.weapon_desc, 5, 100.0)
        self.assertEqual(leather_weapon.attack, 5)
    
    def test_damage_weapon_normal(self):
        leather_weapon = Weapon(self.weapon_name, 1.0, 5, self.weapon_desc, 5, 100.0)
        self.assertEqual(leather_weapon.damage_weapon(43.0), 100.0-43.0)

    def test_damage_weapon_zero(self):
        leather_weapon = Weapon(self.weapon_name, 1.0, 5, self.weapon_desc, 5, 100.0)
        self.assertEqual(leather_weapon.damage_weapon(0), 100.0)

    def test_damage_weapon_big(self):
        leather_weapon = Weapon(self.weapon_name, 1.0, 5, self.weapon_desc, 5, 100.0)
        self.assertEqual(leather_weapon.damage_weapon(5000.0), 0)

    def test_repair_weapon_normal(self):
        leather_weapon = Weapon(self.weapon_name, 1.0, 5, self.weapon_desc, 5, 100.0)
        leather_weapon.durability = 30.0
        self.assertEqual(leather_weapon.repair_weapon(26.0), 30.0+26.0)

    def test_repair_weapon_zero(self):
        leather_weapon = Weapon(self.weapon_name, 1.0, 5, self.weapon_desc, 5, 100.0)
        leather_weapon.durability = 91.0
        self.assertEqual(leather_weapon.repair_weapon(0), 91.0)

    def test_repair_weapon_big(self):
        leather_weapon = Weapon(self.weapon_name, 1.0, 5, self.weapon_desc, 5, 100.0)
        leather_weapon.durability = 80.0
        self.assertEqual(leather_weapon.repair_weapon(5000.0), 100.0)

if __name__ == '__main__':
    unittest.main()