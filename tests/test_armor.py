#!/usr/local/bin/python3

import unittest
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from lib.armor import Armor

class TestArmor(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestArmor, self).__init__(*args, **kwargs)
        self.armor_name = 'Leather Armor'
        self.armor_desc = 'A simple studded leather chest harness.'

    def test_init_durability(self):
        leather_armor = Armor(self.armor_name, 5.0, 10, self.armor_desc, 10, 100.0)
        self.assertEqual(leather_armor.durability, 100.0)

    def test_init_max_durability(self):
        leather_armor = Armor(self.armor_name, 5.0, 10, self.armor_desc, 10, 100.0)
        self.assertEqual(leather_armor.max_durability, 100.0)

    def test_init_status(self):
        leather_armor = Armor(self.armor_name, 5.0, 10, self.armor_desc, 10, 100.0)
        self.assertEqual(leather_armor.status.value, 'REPAIRED')

    def test_init_defense(self):
        leather_armor = Armor(self.armor_name, 5.0, 10, self.armor_desc, 10, 100.0)
        self.assertEqual(leather_armor.defense, 10)
    
    def test_damage_armor_normal(self):
        leather_armor = Armor(self.armor_name, 5.0, 10, self.armor_desc, 10, 100.0)
        self.assertEqual(leather_armor.damage_armor(43.0), 100.0-43.0)

    def test_damage_armor_zero(self):
        leather_armor = Armor(self.armor_name, 5.0, 10, self.armor_desc, 10, 100.0)
        self.assertEqual(leather_armor.damage_armor(0), 100.0)

    def test_damage_armor_big(self):
        leather_armor = Armor(self.armor_name, 5.0, 10, self.armor_desc, 10, 100.0)
        self.assertEqual(leather_armor.damage_armor(5000.0), 0)

    def test_repair_armor_normal(self):
        leather_armor = Armor(self.armor_name, 5.0, 10, self.armor_desc, 10, 100.0)
        leather_armor.durability = 30.0
        self.assertEqual(leather_armor.repair_armor(26.0), 30.0+26.0)

    def test_repair_armor_zero(self):
        leather_armor = Armor(self.armor_name, 5.0, 10, self.armor_desc, 10, 100.0)
        leather_armor.durability = 91.0
        self.assertEqual(leather_armor.repair_armor(0), 91.0)

    def test_repair_armor_big(self):
        leather_armor = Armor(self.armor_name, 5.0, 10, self.armor_desc, 10, 100.0)
        leather_armor.durability = 80.0
        self.assertEqual(leather_armor.repair_armor(5000.0), 100.0)

if __name__ == '__main__':
    unittest.main()