CREATE TABLE IF NOT EXISTS items
(
    item_id INTEGER PRIMARY KEY,
    weight FLOAT,
    value FLOAT,
    description TEXT
);

CREATE TABLE IF NOT EXISTS weapons
(
    weapon_id INTEGER PRIMARY KEY,
    weight FLOAT,
    value FLOAT,
    description TEXT,
    attack FLOAT,
    durability FLOAT,
    max_durability FLOAT,
    status TEXT
);

CREATE TABLE IF NOT EXISTS armor
(
    armor_id INTEGER PRIMARY KEY,
    weight FLOAT,
    value FLOAT,
    description TEXT,
    defense FLOAT,
    durability FLOAT,
    max_durability FLOAT,
    status TEXT
);

CREATE TABLE IF NOT EXISTS tiles
(
    tile_id INTEGER PRIMARY KEY,
    name TEXT,
    tile_x INTEGER,
    tile_y INTEGER,
    description TEXT,
    items TEXT,
    enemies TEXT,
    npcs TEXT,
    exits TEXT,
    is_locked INTEGER,
    lock_message TEXT
);

CREATE TABLE IF NOT EXISTS enemies
(
    id INTEGER PRIMARY KEY, 
    name TEXT, 
    max_hp INTEGER, 
    max_mp INTEGER, 
    strength INTEGER, 
    intelligence INTEGER, 
    stamina INTEGER, 
    speed INTEGER, 
    xp INTEGER, 
    gold INTEGER, 
    attack INTEGER,
    defense INTEGER,
    min_atk FLOAT,
    max_atk FLOAT,
    min_def FLOAT,
    max_def FLOAT
);