#!/usr/local/bin/python3

from lib.status import Status
from lib.inventory import Inventory

class Entity:

    def __init__(self, name, age, health, max_health, magic, max_magic):
        
        self.id = 0
        self.name = name
        self.age = age
        self.level = 1
        self.health = health
        self.max_health = max_health
        self.magic = magic
        self.max_magic = max_magic
        self.status = Status.alive
        self.backpack = Inventory()

    def reduce_health(self, health_to_reduce):
        if self.health - health_to_reduce <= 0:
            self.health = 0
        else:
            self.health = self.health - health_to_reduce
        return self.health

    def add_health(self, health_to_add):
        if self.health + health_to_add >= self.max_health:
            self.health = self.max_health
        else:
            self.health += health_to_add
        return self.health

    def reduce_magic(self, magic_to_reduce):
        if self.magic - magic_to_reduce <= 0:
            self.magic = 0
        else:
            self.magic = self.magic - magic_to_reduce
        return self.magic

    def add_magic(self, magic_to_add):
        if self.magic + magic_to_add >= self.max_magic:
            self.magic = self.max_magic
        else:
            self.magic += magic_to_add
        return self.magic