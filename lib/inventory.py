#!/usr/local/bin/python3

class Inventory:

    def __init__(self):
        self.gold = 0
        self.items = []

    def drop_item(self, item_name):
        item_found = False
        for item in self.items:
            if item_name.lower() == item.name.lower():
                self.items.remove(item)
                item_found = True
                print('The item: {0} has been dropped.'.format(item_name.lower()))
        if(item_found == False):
            print('You don\'t seem to have the item: {0}'.format(item_name.lower()))
    
    def __str__(self):
        print_str = '\nINVENTORY\n====================\nMONEY: {0}\n====================\n'.format(self.gold)
        for item in self.items:
            print_str += str(item) + '\n'
        return print_str