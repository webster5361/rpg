#!/usr/local/bin/python3

from lib.entity import Entity
from lib.spellbook import SpellBook

class Player(Entity):
    
    def __init__(self, name, age, health, max_health, magic, max_magic):
        super().__init__(name, age, health, max_health, magic, max_magic)
        
        self.strength = 10
        self.intelligence = 10
        self.stamina = 10
        self.speed = 15

        self.stat_points = 0

        self.xp = 0
        self.xp_to_next_level = self.calculate_next_level_xp(self.level+1)

        # Current Location
        self.player_x = 0
        self.player_y = 0

        self.equipped_weapon = None
        self.equipped_armor = None
        self.equipped_helmet = None
        self.equipped_gloves = None
        self.equipped_boots = None

        self.spellbook = SpellBook()
        self.in_battle = False
        self.calculate_max_health()
        self.calculate_max_magic()

    def calculate_max_health(self):
        self.max_health = int((self.stamina + (self.strength / 2) * self.level) * 5.5)
        self.health = self.max_health

    def calculate_max_magic(self):
        self.max_magic = int((self.intelligence + (self.stamina / 2) * self.level) * 3)
        self.magic = self.max_magic

    def calculate_next_level_xp(self, level):
        self.xp_to_next_level = 50 * (level ** 2) - (50 * level)

    def calculate_attack_power(self):
        return (self.strength + self.equipped_weapon.attack) * 2

    def calculate_magic_attack_power(self):
        return (self.intelligence * 3.5)

    def calculate_armor_rating(self):
        armor_rating = 0
        if self.equipped_armor:
            armor_rating += self.equipped_armor.defense
        if self.equipped_helmet:
            armor_rating += self.equipped_helmet.defense
        if self.equipped_gloves:
            armor_rating += self.equipped_gloves.defense
        if self.equipped_boots:
            armor_rating += self.equipped_boots.defense
        return armor_rating

    def add_stat_point(self, stat_to_increase, number_of_points):
        if stat_to_increase == 'strength':
            self.strength += number_of_points
            return True
        elif stat_to_increase == 'intelligence':
            self.intelligence += number_of_points
            return True
        elif stat_to_increase == 'stamina':
            self.stamina += number_of_points
            return True
        else:
            return False

    def __str__(self):
        print_str = '\nCHARACTER SHEET\n=========================================\nName: ' \
            '{0}\nAge: {1}\nLevel: {2}\nXP: {3} / {4}\nStrength: {5}\nIntelligence: ' \
            '{6}\nStamina: {7}\nHealth: {8} / {9}\nMagic: {10} / {11}\n\n'.format(self.name, self.age, self.level, self.xp, \
            self.xp_to_next_level, self.strength, self.intelligence, self.stamina, self.health, self.max_health, self.magic, self.max_magic)
        return print_str