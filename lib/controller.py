#!/usr/local/bin/python3

from lib.game_logger import GameLogger
from lib.entity import Entity
from lib.enemy import Enemy
from lib.npc import NPC
from lib.player import Player
from lib.item import Item
from lib.weapon import Weapon
from lib.armor import Armor
from lib.world import World
from lib.tile import Tile
from lib.spell import Spell
from lib.type import Type
from lib.database import Database
from lib.battle_manager import BattleManager
from lib.enemy_generator import EnemyGenerator
import sys
import json

class Controller:

    def __init__(self):
        self.logger = GameLogger()
        self.logger.log_string('Game Log file created... [./logs/game.log]', 'info')
        self.player = None
        self.game_over = False
        self.world = World()
        self.current_tile = None
        self.database = Database()
        self.database.create_connection(self)
        self.battle_manager = BattleManager()
        self.enemy_generator = EnemyGenerator(self)

    def print_banner(self):
        temp1 = '#                                                           #'
        temp2 = '#############################################################'
        print()
        print(temp2)
        print(temp1)
        print('#                          The Game                         #')
        print(temp1)
        print(temp1)
        print(temp2)
        print()

    def main_menu(self):
        print()
        print('1.    New Game')
        print('2.    Load Game')
        print('3.    Options')
        print('9.    Exit Game')
        print()

    def game_intro(self):
        self.print_banner()
        loop = 1
        choice = 0
        try:
            while loop == 1:
                self.main_menu()
                try:
                    choice = int(input('>   '))
                except ValueError:
                    print('')
                if choice == 1:
                    loop = 0
                    print('Starting a new game')
                    self.new_game()
                elif choice == 2:
                    loop = 0
                    self.load_game()
                elif choice == 3:
                    loop = 0
                    self.game_options()
                elif choice == 9:
                    loop = 0
                    sys.exit()
                else:
                    loop = 1
                    print('\n\nyou have made an invliad choice. Try again.\n\n')
        except KeyboardInterrupt:
            self.logger.log_string('User pressed CTRL+C. Exiting game...', 'info')
            print('\nUser pressed CTRL+C. Exiting game...\n\n')
            sys.exit(0)

    def new_game(self):
        self.logger.log_string('Creating new game...', 'info')
        print('Let\'s start a new game together!')
        self.create_player()
        self.logger.log_string('Character created!', 'info')
        self.create_starting_equipment()
        self.logger.log_string('Character\'s starting equipment created...', 'info')
        self.create_starting_spellbook()
        self.logger.log_string('Character\'s starting spellbook has been filled.', 'info')
        self.build_game_world()
        self.place_player_in_world()

    
    def create_player(self):
        player_name = input('What is your player\'s name?\n> ')
        player_age = input('How old are you?\n> ')
        health = 50
        magic = 50
        self.player = Player(player_name, player_age, health, health, magic, magic)
        self.player.calculate_next_level_xp(self.player.level+1)
        self.battle_manager.player = self.player

    def create_starting_equipment(self):
        self.player.backpack.gold = 20
        
        # dagger
        dagger = Weapon('Simple Dagger', 1.0, 5, 'A simple, rusted dagger.', 5, 100.0)
        self.logger.log_string('Created starting weapon: \n{0}'.format(dagger), 'info')
        self.player.equipped_weapon = dagger

        # leather armor
        leather_armor = Armor('Leather Armor', 5.0, 10, 'A simple studded leather chest harness.', 10, 100.0)
        self.logger.log_string('Created starting armor: \n{0}'.format(leather_armor), 'info')
        self.player.equipped_armor = leather_armor

        # key
        simple_key = Item('Simple Key', 0.5, 0, 'A key. You have no idea what sort of lock it fits.')
        self.logger.log_string('Created starting item: \n{0}'.format(simple_key), 'info')
        self.player.backpack.items.append(simple_key)

        # rock
        rock = Item('Rock', 1.0, 0, 'A rock. Pretty self-explanatory.')
        self.logger.log_string('Created starting item: \n{0}'.format(rock), 'info')
        self.player.backpack.items.append(rock)

    def create_starting_spellbook(self):
        starting_heal_spell = Spell('Cure', Type.healing, 3, 10, 'The most basic healing spell.')
        self.logger.log_string('Created starting spell: \n{0}'.format(starting_heal_spell), 'info')
        self.player.spellbook.spells.append(starting_heal_spell)
        starting_damage_spell = Spell('Fireball', Type.damage, 4, 10, 'The most basic damage spell.')
        self.logger.log_string('Created starting spell: \n{0}'.format(starting_damage_spell), 'info')
        self.player.spellbook.spells.append(starting_damage_spell)

    def build_game_world(self):
        world_rows = self.database.select_all('tiles')
        for tile_id, name, tile_x, tile_y, description, items, enemies, npcs, exits, is_locked, lock_message in world_rows:
            self.logger.log_string('LOADING WORLD SPACE: [ {0} ] {1} : {2} ( {3} , {4} )'.format(tile_id, name, description, tile_x, tile_y), 'info')
            new_tile = Tile()
            new_tile.id = tile_id
            new_tile.name = name
            new_tile.x = tile_x
            new_tile.y = tile_y
            new_tile.description = description
            new_tile.items = items
            new_tile.enemies = enemies
            new_tile.npcs = npcs
            exit_parts = exits.split(',')
            for part in exit_parts:
                part_split = part.split('(')
                direction = part_split[0]
                tile_id = int(part_split[1].replace(')', ''))
                new_exit = (direction, tile_id)
                new_tile.exits.append(new_exit)
            new_tile.is_locked = is_locked
            new_tile.lock_message = lock_message
            self.world.map.append(new_tile)
        self.logger.log_string('World building is complete.', 'info')

    def place_player_in_world(self):
        self.world.move_to(0, 0, self.player, self)
        self.logger.log_string('Placed character in the world at location [ {0} , {1} ]'.format('0', '0'), 'info')
        self.update_current_tile(0,0)
    
    def update_current_tile(self, x, y):
        self.current_tile = self.world.find_tile(x,y)

    def parse_cast(self, user_input_parts):
        if len(user_input_parts) == 1:
            print('what would you like to cast?')
            for i in range(len(self.player.spellbook.spells)):
                print('[ {0} ] : {1} ( {2} MP )'.format(i+1, self.player.spellbook.spells[i].name, self.player.spellbook.spells[i].cost))
            spell_choice = int(input('>   '))
            self.player.spellbook.spells[spell_choice-1].cast_spell(self.player, user_input_parts)
        else:
            for spell in self.player.spellbook.spells:
                self.cast_spell(spell, user_input_parts)
    
    def cast_spell(self, spell, user_input_parts):
        if user_input_parts[1].lower() == spell.name.lower():
            if self.player.health == self.player.max_health and spell.spell_type == Type.healing:
                print('You are already at full health. No need to heal yourself now.')
            else:
                if spell.spell_type == Type.healing and self.player.magic >= spell.cost:
                    amount_of_heal = spell.cast(self.player)
                    self.player.magic -= spell.cost
                    self.player.add_health(amount_of_heal)
                else:
                    print('You are not in battle at the moment...')
        else:
            print('spell not found...')


    def parse_user_input(self, user_input):
        user_input = user_input.lower()
        self.logger.log_string('User input command: {0}'.format(user_input), 'info')
        temp = self.world.find_tile(self.player.player_x, self.player.player_y)
        user_input_parts = user_input.split(' ')
        if user_input_parts[0] in ['look', 'describe']:
            print(temp)
        elif user_input_parts[0] in ['fight', 'attack']:
            enemies = temp.enemies;
            if(enemies == '[{}]'):
                print('There are no enemies to fight here... sad...')
        elif user_input_parts[0] in ['move', 'go']:
            self.parse_move(user_input_parts)
        elif user_input_parts[0] in ['player', 'stats', 'character']:
            print(self.player)
        elif user_input_parts[0] in ['inventory', 'inv', 'bag', 'backpack']:
            print(self.player.backpack)
        elif user_input_parts[0] in ['drop', 'destroy', 'remove']:
            self.player.backpack.drop_item(user_input_parts[1])
        elif user_input_parts[0] in ['cast']:
            self.parse_cast(user_input_parts)
        elif user_input_parts[0] in ['damage']:
            self.parse_damage(user_input_parts)
        elif user_input in ['exit', 'quit', 'stop']:
            self.exit_game()

    def parse_damage(self, user_input_parts):
        self.player.reduce_health(int(user_input_parts[1]))

    def check_direction(self, desired_direction):
        for exit in self.current_tile.exits:
            # [0] is the direction
            # [1] is the tile ID
            if desired_direction == exit[0]:
                return True
        return False

    def get_direction_id(self, desired_direction):
        temp_id = ''
        for exit in self.current_tile.exits:
            if desired_direction == exit[0].lower():
                temp_id = exit[1]
        return temp_id

    def get_desired_direction(self, user_input_parts):
        desired_direction = ''
        if user_input_parts in ['n', 'north', 'up']:
                desired_direction = 'N'
        elif user_input_parts in ['s', 'south', 'down']:
                desired_direction = 'S'
        elif user_input_parts in ['e', 'east', 'right']:
                desired_direction = 'E'
        elif user_input_parts in ['w', 'west', 'left']:
                desired_direction = 'W'
        else:
            print('I am not sure where you are trying to go...')
            print('Acceptable directions are: \"north\", \"south\", \"east\", \"west\"\n')
        return desired_direction

    def parse_move(self, user_input_parts):
        if len(user_input_parts) == 1:
            print('Where would you like to go?')
        else:
            desired_direction = self.get_desired_direction(user_input_parts[1])  
            if self.check_direction(desired_direction):
                self.world.move_to_id(self.get_direction_id(user_input_parts[1]), self.player, self)
                battle_check = self.battle_manager.check_random_battle()
                self.logger.log_string('Random battle check: {0}'.format(battle_check), 'info')
                if battle_check < 40:
                    print('Random battle initiated.')
                    enemies = self.battle_manager.generate_random_enemies(self, self.player)
                    self.battle_manager.battle(self, self.player, enemies)
            else:
                print('You can\'t go that way...')

    def exit_game(self):
        while self.game_over == False:
            print('Are you sure? [ y / n ]')
            confirmation = input('>   ')
            confirmation = confirmation.lower()
            if confirmation in ['y', 'yes']:
                self.logger.log_string('User confirmed desire to quit game... exiting now...', 'info')
                sys.exit()
            elif confirmation in ['n', 'no']:
                break
            else:
                print('INVALID INPUT: Please only choose \'y\' or \'n\'')
                self.logger.log_string('INVALID INPUT DETECTED', 'warn')

    def load_game(self):
        raise NotImplementedError

    def save_game(self):
        raise NotImplementedError

    def game_options(self):
        raise NotImplementedError