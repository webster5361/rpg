#!/usr/local/bin/python3

from lib.entity import Entity
from lib.status import Status
import random

class Enemy(Entity):
    
    def __init__(self, name, age, health, max_health, magic, max_magic):
        super().__init__(name, age, health, max_health, magic, max_magic)

        self.strength = 10
        self.intelligence = 10
        self.stamina = 10
        self.speed = 10
        self.xp_dropped = self.calculate_xp_dropped()
        self.attack = 4
        self.defense = 2
        self.atk_min = 1.2
        self.atk_max = 3.5
        self.def_min = 1.1
        self.def_max = 2.8

    def calculate_xp_dropped(self):
        return 2 * self.level * 10

    def calculate_attack(self):
        return self.attack + (self.strength / 2) + self.level

    def calculate_defense(self):
        return self.defense + (self.strength / 2) + self.level

    def calculate_max_hp(self):
        return self.stamina + (self.strength / 2) * self.level

    def calculate_max_mp(self):
        return self.intelligence + (self.stamina / 2) * self.level

    def level_up(self):
        self.level += 1
        self.strength += 1
        self.intelligence += 1
        self.stamina += 1
        if self.level % 5 == 0:
            self.speed += 1

    def set_gold_to_drop(self, gold):
        self.backpack.gold = gold
    
    def calculate_attack_power(self):
        return int((self.attack + ((self.level + self.strength) + random.uniform(self.atk_min, self.atk_max))))

    def calculate_defense_power(self):
        return int((self.defense + ((self.level + self.strength) + random.uniform(self.def_min, self.def_max))))

    def __str__(self):
        if self.status == Status.alive:
            print_str = 'Name: {0} : HP: {1} / {2}'.format(self.name, self.health, self.max_health)
        else:
            print_str = 'Name: {0} : HP: DEAD'.format(self.name)
        return print_str