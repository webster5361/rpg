#!/usr/local/bin/python3

class Tile:

    def __init__(self):
        self.id = 0
        self.name = ''
        self.x = 0
        self.y = 0
        self.visited = False
        self.description = ''
        self.items = []
        self.enemies = []
        self.npcs = []
        self.exits = []
        self.is_locked = False
        self.lock_message = ''

    def __str__(self):
        return '\n\nROOM: \n[ {0} ] {1}\n[ {2} , {3} ]\n\n{4}\n\nENEMIES:\n{5}\nNPCs:\n{6}\nITEMS:\n{7}\nEXITS:\n{8}'.format(self.id, self.name, self.x, self.y, self.description, self.enemies, self.npcs, self.items, self.exits)