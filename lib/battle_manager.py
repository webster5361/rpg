#!/usr/local/bin/python3

import random
import sys
from lib.status import Status
from lib.enemy import Enemy
from lib.type import Type
import time

class BattleManager:

    def __init__(self):
        self.player = None
        self.xp_awarded = 0
        self.gold_awarded = 0

    def check_random_battle(self):
        # let's say 40%
        random.seed(random.randint(0, 1000))
        battle_test = random.randint(0, 100)
        return battle_test

    def all_enemies_dead(self, enemies):
        for enemy in enemies:
            if enemy.status == Status.alive:
                return False
        return True

    def battle_menu(self, player, enemies):
        sep = '=====================\n'
        print('Player:\n{0}'.format(sep))
        print('Health: {0} / {1}\nMagic: {0} / {1}'.format(player.health, player.max_health, player.magic, player.max_magic))
        print(sep)
        print('Enemies:\n{0}'.format(sep))
        for enemy in enemies:
            print(enemy)
        print(sep)
        print('1.  Attack')
        print('2.  Cast')
        print('3.  Item')
        print('4.  Run')
        choice = (input('>   '))
        return choice

    def damage_enemy(self, player, enemies, i):
        player_attack = player.calculate_attack_power()
        if enemies[i].health - player_attack <= 0:
            enemies[i].health = 0
            enemies[i].status = Status.dead
            print('You damage {0} for {1} damage.'.format(enemies[i].name, player_attack))
            print('The {0} has died...'.format(enemies[i].name))
            self.xp_awarded += enemies[i].xp_dropped
            self.gold_awarded += enemies[i].backpack.gold
        else:
            enemies[i].health -= player_attack
            print('You damage {0} for {1} damage.'.format(enemies[i].name, player_attack))
        return enemies

    def damage_player(self, enemy, player):
        enemy_attack = enemy.calculate_attack_power()
        player_armor_rating = player.calculate_armor_rating()
        damage_multiplier = enemy_attack / (enemy_attack + player_armor_rating)
        total_damage = int(enemy_attack * damage_multiplier)
        if player.health - total_damage <= 0:
            player.status = Status.dead
            player.health = 0
            player.in_battle = False
        else:
            player.health -= total_damage
            print('The {0} has damaged you for {1} points of damage. Your health is now: {2}'.format(enemy.name, enemy_attack, player.health))

    def all_damage_player(self, enemies, player):
        for enemy in enemies:
            if enemy.status != Status.dead:
                enemy_attack = enemy.calculate_attack_power()
                player_armor_rating = player.calculate_armor_rating()
                damage_multiplier = enemy_attack / (enemy_attack + player_armor_rating)
                total_damage = int(enemy_attack * damage_multiplier)
                if player.health - total_damage <= 0:
                    player.status = Status.dead
                    player.health = 0
                    player.in_battle = False
                else:
                    player.health -= total_damage
                    print('The {0} has damaged you for {1} points of damage. Your health is now: {2}'.format(enemy.name, enemy_attack, player.health))
            time.sleep(0.75)

    def parse_attack(self, player, enemies):
        print('Enemies:\n=====================\n')
        for i in range(len(enemies)):
            print('[ {0} ] {1}'.format(i+1, enemies[i]))
        print('=====================\n')
        enemy_choice = int(input('>   '))
        enemies = self.damage_enemy(player, enemies, enemy_choice-1)
        if self.all_enemies_dead(enemies):
            player.in_battle = False
        for enemy in enemies:
            if enemy.status != Status.dead:
                self.damage_player(enemy, player)
            time.sleep(0.75)


    def parse_cast(self, player, enemies):
        for i in range(len(player.spellbook.spells)):
            print('[ {0} ] : {1} ( {2} MP )'.format(i+1, player.spellbook.spells[i].name, player.spellbook.spells[i].cost))
        spell_choice = int(input('>   '))
        if player.spellbook.spells[spell_choice-1].spell_type == Type.healing and player.magic >= player.spellbook.spells[spell_choice-1].cost:
            amount_of_heal = player.spellbook.spells[spell_choice-1].cast(player)
            player.magic -= player.spellbook.spells[spell_choice-1].cost
            player.add_health(amount_of_heal)
        elif player.spellbook.spells[spell_choice-1].spell_type == Type.damage and player.magic >= player.spellbook.spells[spell_choice-1].cost:
            print('Which enemy are you going to target?')
            print('Enemies:\n=====================\n')
            for i in range(len(enemies)):
                print('[ {0} ] {1}'.format(i+1, enemies[i]))
            print('=====================\n')
            enemy_choice = int(input('>   '))
            enemies = self.damage_enemy_spell(player, enemies, enemy_choice-1, player.spellbook.spells[spell_choice-1])
        self.all_damage_player(enemies, player)

    def damage_enemy_spell(self, player, enemies, i, spell):
        player_attack = player.calculate_magic_attack_power()
        if enemies[i].health - player_attack <= 0:
            enemies[i].health = 0
            enemies[i].status = Status.dead
            player.magic -= spell.cost
            print('You damage {0} for {1} damage with your spell.'.format(enemies[i].name, player_attack))
            print('The {0} has died...'.format(enemies[i].name))
            self.xp_awarded += enemies[i].xp_dropped
            self.gold_awarded += enemies[i].backpack.gold
        else:
            enemies[i].health -= player_attack
            player.magic -= spell.cost
            print('You damage {0} for {1} damage with your spell.'.format(enemies[i].name, player_attack))
        return enemies

    def parse_item(self):
        raise NotImplementedError

    def parse_run(self, enemies, player):
        # let's say 30%
        random.seed(random.randint(0, 1000))
        run_test = random.randint(0, 100)
        if run_test <= 30:
            player.in_battle = False
            print('You were able to get away...')
        else:
            self.all_damage_player(enemies, player)

    def battle(self, controller, player, enemies):
        try:
            player.in_battle = True
            self.xp_awarded = 0
            self.gold_awarded = 0
            while player.in_battle:
                battle_choice = self.battle_menu(player, enemies)
                if battle_choice.lower() in ['1', 'attack']:
                    self.parse_attack(player, enemies)
                elif battle_choice in ['2', 'cast']:
                    self.parse_cast(player, enemies)
                elif battle_choice in ['3', 'item']:
                    self.parse_item()
                elif battle_choice.lower() in ['4', 'run']:
                    self.parse_run(enemies, player)                      
                else:
                    print('error')
            if player.status == Status.alive:
                print('The battle has ended. you win.')
                print('Player awarded {0} XP.'.format(self.xp_awarded))
                player.xp += self.xp_awarded
                print('Player awarded {0} Gold.'.format(self.gold_awarded))
                player.backpack.gold += self.gold_awarded
            else:
                print('The battle has ended. You have died...')
                controller.game_intro()
        except KeyboardInterrupt:
            controller.logger.log_string('User interrupted game process { CTRL + C }.', 'info')
            sys.exit(0)

    def generate_random_enemies(self, controller, player):
        number_enemies = random.randint(1, 3)
        enemies_to_battle = []
        print(number_enemies)
        for i in range(number_enemies):
            controller.logger.log_string('Added enemy to battle ({0})...'.format(i))
            enemies_to_battle.append(controller.enemy_generator.generate_enemy(player))
        return enemies_to_battle
