#!/usr/local/bin/python3

from lib.item import Item
from lib.equipment_status import EquipmentStatus

class Weapon(Item):

    def __init__(self, name, weight, value, description, attack, max_durability):
        super().__init__(name, weight, value, description)

        self.attack = attack
        self.durability = max_durability
        self.max_durability = max_durability
        self.status = EquipmentStatus.repaired

    def damage_weapon(self, amount_to_damage):
        if self.durability - amount_to_damage <= 0:
            self.durability = 0
            self.check_is_broken()
        else:
            self.durability = self.durability - amount_to_damage
        return self.durability

    def repair_weapon(self, amount_to_repair):
        if self.durability + amount_to_repair >= self.max_durability:
            self.durability = self.max_durability
        else:
            self.durability += amount_to_repair
        return self.durability

    def check_is_broken(self):
        if self.durability <= 0:
            self.status = EquipmentStatus.broken
            return True
        else:
            return False

    def __str__(self):
        return '\n{0}\n{1} lbs.\n{2} gold\n{3}\n{4}/{5}\n{6}\n'.format(self.name, self.weight, self.value, self.attack, self.durability, self.max_durability, self.description)