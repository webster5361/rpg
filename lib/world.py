#!/usr/local/bin/python3

from lib.tile import Tile
class World:

    def __init__(self):
        self.map = []
        self.player_location_x = 0
        self.player_location_y = 0
    

    def find_tile(self, x, y):
        temp_tile = Tile()
        for tile in self.map:
            if (tile.x == x and tile.y == y):
                temp_tile = tile
        return temp_tile

    def find_tile_id(self, id):
        temp_tile = Tile()
        for tile in self.map:
            if(tile.id == id):
                temp_tile = tile
        return temp_tile

    def move_to(self, x, y, player, controller):
        player.player_x = x
        player.player_y = y
        location = self.find_tile(x, y)
        controller.current_tile = location
        print(location.description)

    def move_to_id(self, id, player, controller):
        new_tile = self.find_tile_id(id)
        if new_tile.is_locked:
            print(new_tile.lock_message)
        else:
            player.player_x = new_tile.x
            player.player_y = new_tile.y
            controller.current_tile = new_tile
            print(new_tile.description)

    def __str__(self):
        print_str = 'TILE_MAP\n===========================================\n'
        for tile in self.map:
            print_str += ('[ {0} , {1} ]\nNAME: {2}\nDESC: {3}\n\n'.format(tile.x, tile.y, tile.name, tile.description))
        return print_str
