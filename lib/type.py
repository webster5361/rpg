#!/usr/local/bin/python3

from enum import Enum

class Type(Enum):
    damage = 'DAMAGE'
    healing = 'HEALING'