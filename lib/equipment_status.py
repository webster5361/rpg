#!/usr/local/bin/python3

from enum import Enum

class EquipmentStatus(Enum):
    broken = 'BROKEN'
    repaired = 'REPAIRED'