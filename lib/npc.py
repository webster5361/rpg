#!/usr/local/bin/python3

from lib.entity import Entity

class NPC(Entity):
    
    def __init__(self, name, age, health, max_health, magic, max_magic):
        super().__init__(name, age, health, max_health, magic, max_magic)