#!/usr/local/bin/python3

from lib.enemy import Enemy
from lib.status import Status
import random

class EnemyGenerator:

    def __init__(self, controller):
        self.controller = controller
        self.enemy_encyclopedia = []
        self.load_encyclopedia()

    def load_encyclopedia(self):
        enemies_rows = self.controller.database.select_all('enemies')
        for id, name, max_hp, max_mp, strength, intelligence, stamina, speed, xp, gold, attack, defense, min_atk, max_atk, min_def, max_def in enemies_rows:
            self.controller.logger.log_string('LOADING ENEMY INTO ENCYCLOPEDIA: [ {0} ] Name: {1} - Health: {2} - Magic: {3} - Str: {4} - Int: {5} - Stam: {6} - Spd: {7} - XP: {8} - Gold: {9} - Attack: {10} - Defense: {11} - Min. Atk: {12} - Max. Atk: {13} - Min. Def: {14} - Max. Def: {15}'.format(id, name, max_hp, max_mp, strength, intelligence, stamina, speed, xp, gold, attack, defense, min_atk, max_atk, min_def, max_def), 'info')
            base_enemy = Enemy(name, 10, max_hp, max_hp, max_mp, max_mp)
            base_enemy.id = id
            base_enemy.strength = strength
            base_enemy.intelligence = intelligence
            base_enemy.stamina = stamina
            base_enemy.speed = speed
            base_enemy.xp_dropped = xp
            base_enemy.backpack.gold = gold
            base_enemy.status = Status.alive
            base_enemy.attack = attack
            base_enemy.defense = defense
            base_enemy.atk_min = min_atk
            base_enemy.atk_max = max_atk
            base_enemy.def_min = min_def
            base_enemy.def_max = max_def
            self.enemy_encyclopedia.append(base_enemy)
        self.controller.logger.log_string('All enemies loaded...', 'info')

    def generate_random_age(self):
        random.seed(random.randint(0, 1000))
        age = random.randint(10, 90)
        return age

    def generate_level(self, player_level):
        random.seed(random.randint(0, 1000))
        if player_level - 2 <= 0:
            return random.randint(1, player_level + 2)
        elif player_level + 2 > 99:
            return random.randint(player_level - 2, 99)
        else:
            return random.randint(player_level - 2, player_level + 2)

    def generate_gold_dropped(self, player_level):
        """[1-10]+Level * 2"""
        return ((random.randint(1,10)+player_level) * 2)

    def generate_enemy(self, player):
        temp_enemy_id = random.randint(0, len(self.enemy_encyclopedia))
        temp_enemy = self.enemy_encyclopedia[temp_enemy_id]
        self.controller.logger.log_string('Random enemy chosen: [ {0} ] : {1}'.format(temp_enemy.id, temp_enemy.name), 'info')
        temp_enemy.backpack.gold = self.generate_gold_dropped(player.level)
        temp_enemy.age = self.generate_random_age()
        new_enemy_level = self.generate_level(player.level)
        for i in range(new_enemy_level):
            temp_enemy.level_up()
            self.controller.logger.log_string('Generated enemy({0})...'.format(i))
        return temp_enemy
    