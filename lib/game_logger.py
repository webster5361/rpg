#!/usr/local/bin/python3

import logging
import logging.handlers
import logging.config

class GameLogger:

    def __init__(self):
        """Initializes the logger functionality of the Helper class."""
        # Configure logger
        logging.config.fileConfig('./logs/logging.conf')
        self.logger = logging.getLogger('rpg')

    def log_string(self, string_to_log, log_level):
        """Logs string based on logLevel."""
        if log_level == 'info':
            self.logger.info(string_to_log)
        elif log_level == 'warn':
            self.logger.warning(string_to_log)
        elif log_level == 'error':
            self.logger.error(string_to_log)
        elif log_level == 'debug':
            self.logger.debug(string_to_log)