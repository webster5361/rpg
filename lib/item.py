#!/usr/local/bin/python3

class Item:

    def __init__(self, name, weight, value, description):
        self.name = name
        self.weight = weight
        self.value = value
        self.description = description
        self.usable = False

    def __str__(self):
        return '\n{0}\n{1} lbs.\n{2} gold\n{3}\n'.format(self.name, self.weight, self.value, self.description)