#!/usr/local/bin/python3

from lib.type import Type
class Spell:

    def __init__(self, name, spell_type, cost, power, description):
        self.name = name
        self.spell_type = spell_type
        self.cost = cost
        self.power = power
        self.description = description

    def cast(self, player):
        if self.spell_type == Type.healing:
            return self.power * (player.intelligence * .75)
        else:
            return self.power * (player.intelligence * .50)

    def __str__(self):
        return '\n{0}\n{1}\n{2}\n{3}\n{4}\n'.format(self.name, self.spell_type.value, self.cost, self.power, self.description)