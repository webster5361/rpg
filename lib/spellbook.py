#!/usr/local/bin/python3

class SpellBook:

    def __init__(self):
        self.spells = []

    def __str__(self):
        print_str = 'SPELL BOOK\n====================\n# of Spells: {0}\n====================\n'.format(len(self.spells))
        for spell in self.spells:
            print_str += str(spell) + '\n'
        return print_str