#!/usr/local/bin/python3

from enum import Enum

class Status(Enum):
    alive = 'ALIVE'
    dead = 'DEAD'
    poisoned = 'POSIONED'
    asleep = 'ASLEEP'