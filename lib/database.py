#!/usr/local/bin/python3

import sqlite3

class Database:

    def __init__(self):
        self.db_file = './database/game.db'
        self.conn = None
        self.cursor = None

    def create_connection(self, controller):
        try:
            controller.logger.log_string('Creating connection to game database...', 'info')
            self.conn = sqlite3.connect(self.db_file)
            self.cursor = self.conn.cursor()
            controller.logger.log_string('Database connection created...', 'info')
        except sqlite3.Error as e:
            controller.logger.log_string('Database connection error: {0}'.format(e), 'error')

    def select_all(self, table_name):
        self.cursor.execute("SELECT * FROM {0}".format(table_name))
        rows = self.cursor.fetchall()
        return rows


    