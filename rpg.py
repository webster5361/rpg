#!/usr/local/bin/python3

from lib.controller import Controller
import sys

def main():
    CONTROLLER = Controller()
    CONTROLLER.game_intro()
    print(CONTROLLER.player.backpack.gold)
    print(CONTROLLER.player.equipped_weapon)
    print(CONTROLLER.player.equipped_armor)
    print(CONTROLLER.player.backpack)
    print(CONTROLLER.player.spellbook)
    print(CONTROLLER.player)
    print(CONTROLLER.world)
    print(CONTROLLER.current_tile)

    try:
        while CONTROLLER.game_over == False:
            user_input = input('>   ')
            CONTROLLER.parse_user_input(user_input)
    except KeyboardInterrupt:
        CONTROLLER.logger.log_string('User interrupted game process { CTRL + C }.', 'info')
        sys.exit(0)

if __name__ == '__main__':
    main()